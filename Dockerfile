FROM adoptopenjdk/openjdk8:debianslim-jre

EXPOSE 25565

# source: https://old.reddit.com/r/feedthebeast/comments/5jhuk9/modded_mc_and_memory_usage_a_history_with_a/
ENV JVM_ARGS "-XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=51 -XX:G1HeapRegionSize=32M"
ENV XMN "128M"
ENV XMS "4G"
ENV XMX "6G"

RUN mkdir /opt/app
RUN mkdir /opt/app/serverfiles

RUN apt-get update
RUN apt-get -y install unzip

COPY start.sh /opt/app
COPY eula.txt /opt/app/serverfiles

CMD ["/bin/sh", "/opt/app/start.sh"]
