#! /bin/sh
# utf-8

cd /opt/app/

echo ""
echo "[EXTRACTING SERVERFILES]"
unzip -n ./serverfiles/*.zip -d ./serverfiles/

cd /opt/app/serverfiles/

echo ""
echo "[ENV VALUES]"
echo "JAVA Version: $JAVA_VERSION"
echo "JVM_ARGS: $JVM_ARGS"
echo "XMN: $XMN"
echo "XMS: $XMS"
echo "XMX: $XMX"

echo ""
echo "[DETECTING FORGE JAR]"
# shellcheck disable=SC2012
minecraft_version=$(ls | sed -rn "s/([FORGEforge]{5})-([0-9.]+)-([0-9.]+)-(\w+)[.]jar/\2/p")
# shellcheck disable=SC2012
forge_version=$(ls | sed -rn "s/([FORGEforge]{5})-([0-9.]+)-([0-9.]+)-(\w+)[.]jar/\3/p")
# shellcheck disable=SC2012
forge_file=$(ls | sed -rn "s/([FORGEforge]{5}-[0-9.]+-[0-9.]+-\w+[.]jar)/\1/p")
echo "Minecraft Version: $minecraft_version"
echo "Forge Version: $forge_version"
echo "Forge file: $forge_file"

echo ""
echo "[SET EXECUTABLE BIT]"
chmod -v +x ./*.jar

echo "$JVM_ARGS" | sed -rn "s/[ ]/\" \"/p"

echo ""
echo "[STARTING SERVER]"
echo "COMMAND: java -Xmn$XMN -Xms$XMS -Xmx$XMX -jar $forge_file nogui $(echo "$JVM_ARGS" | sed -rn "s/[ ]/\" \"/p")"
java "-Xmn$XMN" "-Xms$XMS" "-Xmx$XMX" -jar "$forge_file" nogui "$(echo "$JVM_ARGS" | sed -rn "s/[ ]/\" \"/p")"
