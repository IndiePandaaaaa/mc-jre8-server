# mc-jre8-server

Docker container for a modded Minecraft server, running Java 8.

## Description

This container is used to host a modded Minecraft server within a docker container. A forge powered modpack server will
run inside the container given a zip file for the setup of the server.

## Installation

1. clone the repository

        git clone https://gitlab.com/IndiePandaaaaa/mc-jre8-server.git

2. build an image for the container

        docker build -t mc-jre8-server .

3. download a Minecraft server .zip (e.g. from [curseforge](https://www.curseforge.com/minecraft/search?page=1&pageSize=20&sortType=1&class=modpacks)), make sure it works with Java 8.

4. move it to the folder/volume to bind it to the container

        [optional] $ docker volume create serverfiles

5. run the container

    ```console
    $ docker run -d \
    -p 25565:25565 \
    -v serverfiles:/opt/app/serverfiles \
    mc-jre8-server
    ```

### recommended additional parameters

- limit the maximum usable memory for the container to 2 GB more than `-Xmx`

        -m 8g

## Parameters

- `XMS`: -Xms (jvm argument)
- `XMX`: -Xmx (jvm argument)
- `XMN`: -Xmn (jvm argument)
- `JVM_ARGS`: additional jvm args (e.g. `-XX:+UseG1GC`), overwrites the default values

### container default values

- `XMS`: `4G`
- `XMX`: `6G`
- `XMN`: `128M`
- `JVM_ARGS`: `-XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=51 -XX:G1HeapRegionSize=32M` ([source](https://old.reddit.com/r/feedthebeast/comments/5jhuk9/modded_mc_and_memory_usage_a_history_with_a/))

## Roadmap

- server.properties integration
- server image download from url

## Contributing (undefined)
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License (undefined)
For open source projects, say how it is licensed.